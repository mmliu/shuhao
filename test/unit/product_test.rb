require 'test_helper'

class ProductTest < ActiveSupport::TestCase
  # Replace this with your real tests.
  test "the truth" do
    assert true
  end

  test "product image url must end with jpg||png||gif" do
    ok = %w{fred.gif this.png all.jpg works.Gif }
    bad = %w{however.doc this/one.jp not.moregif} 

    ok.each do |url|
      assert new_product_with_img(url).valid?,  "#{url} should be valid."
    end

    bad.each do |url|
      assert new_product_with_img(url).invalid?,  "#{url} should not be valid."
    end
  end

  def new_product_with_img(image_url)
    #TODO new a product
    Product.new(:title => "ThisIsTitle",
                :description => "Oh haha",
                :image_url => image_url)
  end
end













# == Schema Information
#
# Table name: products
#
#  id                 :integer         not null, primary key
#  title              :string(255)
#  description        :string(255)
#  price              :decimal(, )
#  created_at         :datetime
#  updated_at         :datetime
#  promote_img_name   :string(255)
#  is_for_youth       :boolean         default(FALSE)
#  category_id        :integer
#  author_id          :integer
#  publisher_id       :integer
#  review             :text
#  dangdang_link      :string(255)
#  format             :string(255)
#  pubdate            :string(255)
#  page               :integer
#  brand_id           :integer
#  image_file_name    :string(255)
#  image_content_type :string(255)
#  image_file_size    :integer
#  image_updated_at   :datetime
#

