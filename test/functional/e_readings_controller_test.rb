require 'test_helper'

class EReadingsControllerTest < ActionController::TestCase
  setup do
    @e_reading = e_readings(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:e_readings)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create e_reading" do
    assert_difference('EReading.count') do
      post :create, :e_reading => @e_reading.attributes
    end

    assert_redirected_to e_reading_path(assigns(:e_reading))
  end

  test "should show e_reading" do
    get :show, :id => @e_reading.to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => @e_reading.to_param
    assert_response :success
  end

  test "should update e_reading" do
    put :update, :id => @e_reading.to_param, :e_reading => @e_reading.attributes
    assert_redirected_to e_reading_path(assigns(:e_reading))
  end

  test "should destroy e_reading" do
    assert_difference('EReading.count', -1) do
      delete :destroy, :id => @e_reading.to_param
    end

    assert_redirected_to e_readings_path
  end
end
