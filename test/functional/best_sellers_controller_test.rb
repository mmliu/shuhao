require 'test_helper'

class BestSellersControllerTest < ActionController::TestCase
  setup do
    @best_seller = best_sellers(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:best_sellers)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create best_seller" do
    assert_difference('BestSeller.count') do
      post :create, :best_seller => @best_seller.attributes
    end

    assert_redirected_to best_seller_path(assigns(:best_seller))
  end

  test "should show best_seller" do
    get :show, :id => @best_seller.to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => @best_seller.to_param
    assert_response :success
  end

  test "should update best_seller" do
    put :update, :id => @best_seller.to_param, :best_seller => @best_seller.attributes
    assert_redirected_to best_seller_path(assigns(:best_seller))
  end

  test "should destroy best_seller" do
    assert_difference('BestSeller.count', -1) do
      delete :destroy, :id => @best_seller.to_param
    end

    assert_redirected_to best_sellers_path
  end
end
