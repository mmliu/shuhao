class AddFieldsToProducts < ActiveRecord::Migration
  def self.up
    add_column :products, :format, :string
    add_column :products, :pubdate, :string
    add_column :products, :page, :integer
  end

  def self.down
    remove_column :products, :page
    remove_column :products, :pubdate
    remove_column :products, :format
  end
end
