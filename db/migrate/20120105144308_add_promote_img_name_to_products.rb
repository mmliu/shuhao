class AddPromoteImgNameToProducts < ActiveRecord::Migration
  def self.up
    add_column :products, :promote_img_name, :string
  end

  def self.down
    remove_column :products, :promote_img_name
  end
end
