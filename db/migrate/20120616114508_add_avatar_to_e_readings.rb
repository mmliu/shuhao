class AddAvatarToEReadings < ActiveRecord::Migration
  def self.up
    change_table :e_readings do |t|
      t.has_attached_file :avatar
    end
  end

  def self.down
    drop_attached_file :e_readings, :avatar
  end
end
