class CreateBestSellers < ActiveRecord::Migration
  def self.up
    create_table :best_sellers do |t|
      t.integer :book_id

      t.timestamps
    end
  end

  def self.down
    drop_table :best_sellers
  end
end
