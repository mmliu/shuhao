class CreateEReadings < ActiveRecord::Migration
  def self.up
    create_table :e_readings do |t|
      t.string :name
      t.string :url
      t.string :image

      t.timestamps
    end
  end

  def self.down
    drop_table :e_readings
  end
end
