class CreateBookFlows < ActiveRecord::Migration
  def self.up
    create_table :book_flows do |t|
      t.integer :product_id
      t.string :image_url

      t.timestamps
    end
  end

  def self.down
    drop_table :book_flows
  end
end
