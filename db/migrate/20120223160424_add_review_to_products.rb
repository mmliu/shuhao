class AddReviewToProducts < ActiveRecord::Migration
  def self.up
    add_column :products, :review, :text
  end

  def self.down
    remove_column :products,:review
  end
end
