class AddStoryToBrands < ActiveRecord::Migration
  def change
    add_column :brands, :story, :text
  end
end
