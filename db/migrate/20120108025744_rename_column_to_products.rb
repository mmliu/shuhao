class RenameColumnToProducts < ActiveRecord::Migration
  def self.up
    rename_column :products,:category,:category_id
  end

  def self.down
  end
end
