class AddAuthorIntroToEReadings < ActiveRecord::Migration
  def change
    add_column :e_readings, :author_intro, :text
  end
end
