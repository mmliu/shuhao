class AddDescriptionToBrands < ActiveRecord::Migration
  def self.up
    add_column :brands, :description, :string
  end

  def self.down
    remove_column :brands, :description
  end
end
