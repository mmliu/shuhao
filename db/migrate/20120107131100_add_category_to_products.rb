class AddCategoryToProducts < ActiveRecord::Migration
  def self.up
    add_column :products, :category, :integer
  end

  def self.down
    remove_column :products, :category
  end
end
