class RemoveImageFromEReadings < ActiveRecord::Migration
  def self.up
    remove_column :e_readings,:image
  end

  def self.down
    add_column :e_readings, :image, :string
  end
end
