class AddDangdangLinkToProducts < ActiveRecord::Migration
  def self.up
    add_column :products, :dangdang_link, :string
  end

  def self.down
    remove_column :products, :dangdang_link
  end
end
