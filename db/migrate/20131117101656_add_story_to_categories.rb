class AddStoryToCategories < ActiveRecord::Migration
  def change
    add_column :categories, :story, :text
  end
end
