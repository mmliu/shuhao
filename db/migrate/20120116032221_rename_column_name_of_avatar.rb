class RenameColumnNameOfAvatar < ActiveRecord::Migration
  def self.up
    rename_column :authors,:avatar,:avatar_url
  end

  def self.down
  end
end
