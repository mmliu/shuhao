class AddNameToDocs < ActiveRecord::Migration
  def change
    add_column :docs, :name, :string
  end
end
