class AddAttachmentImageToBrands < ActiveRecord::Migration
  def self.up
    change_table :brands do |t|
      t.has_attached_file :image
    end
  end

  def self.down
    drop_attached_file :brands, :image
  end
end
