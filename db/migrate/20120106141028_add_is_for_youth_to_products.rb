class AddIsForYouthToProducts < ActiveRecord::Migration
  def self.up
    add_column :products, :is_for_youth, :boolean,:default=>false
  end

  def self.down
    remove_column :products, :is_for_youth
  end
end
