class RemoveImageUrlFromBookFlows < ActiveRecord::Migration
  def self.up
    remove_column :book_flows,:image_url
  end

  def self.down
    add_column :book_flows,:image_url,:string
  end
end
