class AddAvatarToAuthor < ActiveRecord::Migration
  def self.up
    add_column :authors, :avatar, :string
  end

  def self.down
    remove_column :authors, :avatar
  end
end
