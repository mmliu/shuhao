class AddImageToBookFlows < ActiveRecord::Migration
  def self.up
    change_table :book_flows do |t|
      t.has_attached_file :image
    end
  end

  def self.down
    drop_attached_file :book_flows, :image
  end
end
