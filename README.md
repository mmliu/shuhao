## How to deploy

1. commit change to bitbucket: `git push bucket master`
2. `cap deploy`
3. done

## How to add image field to model

  https://github.com/thoughtbot/paperclip#quick-start

## How to generate new model

* scaffold: `rails generate scaffold Notice title:string content:text`

or 

* model: `rails generate model Notice title:string content:text`
* controller & view: `rails generate controller Notice index show`

## add field to model
  `rails generate migration add_story_to_brands story:text`
