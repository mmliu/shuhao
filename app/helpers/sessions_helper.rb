module SessionsHelper

  def sign_in(user)
    cookies.permanent.signed[:remember_token] = [user.id,user.salt]
    self.current_user = user # why self. here?
  end

  def current_user=(user) # used for what?
    @current_user = user
  end

  def current_user
    @current_user||=signin_with_salt
  end

  def signed_in?
    !current_user.nil?
  end

  def sign_out
    current_user=nil
    cookies.delete(:remember_token)
  end

  def authenticate
    deny_access unless signed_in?
  end

  def deny_access
    flash[:notice] = "show me who you are..."
    redirect_to signin_path
  end

  private 
    def signin_with_salt
      User.authenticate_with_salt(*cookies.signed[:remember_token]||[nil,nil])
    end
end
