class BookFlowsController < ApplicationController
  def new
    @book_flow = BookFlow.new
  end

  def create
    @book_flow = BookFlow.new(params[:book_flow])

    if @book_flow.save
      render :action => :show
    else
      render :action => :new
    end
  end

  def edit
    @book_flow = BookFlow.find(params[:id])

    render :layout => "admin"
  end

  def update
    @book_flow = BookFlow.find(params[:id])

    if @book_flow.update_attributes(params[:book_flow])
      render :action => :show
    else
      render :action=> :new
    end
  end

  def destroy
    @book_flow = BookFlow.find(params[:id])

    if @book_flow.destroy
      flash[:notice] = 'Item successfully destroied ^-^'
      redirect_to :action => :index
    else
      flash[:notice] = 'Fail to destroy --!'
      redirect_to :action => :index 
    end
  end

  def show
    @book_flow = BookFlow.find(params[:id])
  end

  def index
    render :layout => "admin"
  end
end
