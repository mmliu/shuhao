class ProductsController < ApplicationController
  before_filter :authenticate,:only=>[:new,:create,:edit,:destroy,:update,:index]

  # GET /products
  # GET /products.xml
  def index
    @products = Product.order("updated_at DESC").all

    render :layout => "admin"
    #respond_to do |format|
    #  format.html # index.html.erb
    #  format.xml  { render :xml => @products }
    #end
  end

  def book_list
    list_name = params[:list_name]
    @product_list = Product.where({"#{list_name}" => true}).order("updated_at DESC")

  end

  # GET /products/1
  # GET /products/1.xml
  def show
    @product = Product.find(params[:id])
    @best_sellers = BestSeller.get(7)
    @withReview = Product.pickOneWithReview()
    @notices = Notice.get_notices(6)

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @product }
    end
  end

  # GET /products/new
  # GET /products/new.xml
  def new
    @product = Product.new

    @categories = Category.all

    render :layout => 'admin'
    #respond_to do |format|
    #  format.html # new.html.erb
    #  format.xml  { render :xml => @product }
    #end
  end

  # GET /products/1/edit
  def edit
    @product = Product.find(params[:id])

    render :layout => "admin"
  end

  # POST /products
  # POST /products.xml
  def create
    params.delete("author_id") if params[:author_id] == -1
    params.delete("brand_id") if params[:brand_id] == -1

    @product = Product.new(params[:product])

    respond_to do |format|
      if @product.save
        format.html { redirect_to(@product, :notice => 'Product was successfully created.') }
        format.xml  { render :xml => @product, :status => :created, :location => @product }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @product.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /products/1
  # PUT /products/1.xml
  def update
    #uploaded_io = params[:picture]

    #if uploaded_io != nil
    #  File.open(Rails.root.join('public', 'uploads', uploaded_io.original_filename), 'w') do |file|
    #    file.write(uploaded_io.read)

    #    params[:product][:image_url] = "/uploads/"+ uploaded_io.original_filename
    #  end
    #end
    @product = Product.find(params[:id])

    respond_to do |format|
      if @product.update_attributes(params[:product])
        format.html { redirect_to(@product, :notice => 'Product was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @product.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /products/1
  # DELETE /products/1.xml
  def destroy
    @product = Product.find(params[:id])
    @product.destroy

    respond_to do |format|
      format.html { redirect_to(products_url) }
      format.xml  { head :ok }
    end
  end

  def search
    @best_sellers = BestSeller.get(7)
    @title = "Search for " + params[:q]
    @withReview = Product.pickOneWithReview()

    @products = Product.where("title LIKE ?","%#{params[:q]}%")
  end
end
