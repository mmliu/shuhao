class BestSellersController < ApplicationController
  before_filter :authenticate,:only=>[:index, :new,:create,:edit,:update,:destroy]

  # GET /best_sellers
  # GET /best_sellers.xml
  def index
    @best_sellers = BestSeller.all

    render :layout => "admin"
    #respond_to do |format|
    #  format.html # index.html.erb
    #  format.xml  { render :xml => @best_sellers }
    #end
  end

  # GET /best_sellers/1
  # GET /best_sellers/1.xml
  def show
    @best_seller = BestSeller.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @best_seller }
    end
  end

  # GET /best_sellers/new
  # GET /best_sellers/new.xml
  def new
    @best_seller = BestSeller.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @best_seller }
    end
  end

  # GET /best_sellers/1/edit
  def edit
    @best_seller = BestSeller.find(params[:id])
  end

  # POST /best_sellers
  # POST /best_sellers.xml
  def create
    @best_seller = BestSeller.new(params[:best_seller])

    respond_to do |format|
      if @best_seller.save
        format.html { redirect_to(@best_seller, :notice => 'Best seller was successfully created.') }
        format.xml  { render :xml => @best_seller, :status => :created, :location => @best_seller }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @best_seller.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /best_sellers/1
  # PUT /best_sellers/1.xml
  def update
    @best_seller = BestSeller.find(params[:id])

    respond_to do |format|
      if @best_seller.update_attributes(params[:best_seller])
        format.html { redirect_to(@best_seller, :notice => 'Best seller was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @best_seller.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /best_sellers/1
  # DELETE /best_sellers/1.xml
  def destroy
    @best_seller = BestSeller.find(params[:id])
    @best_seller.destroy

    respond_to do |format|
      format.html { redirect_to(best_sellers_url) }
      format.xml  { head :ok }
    end
  end
end
