class EReadingsController < ApplicationController
  before_filter :authenticate,:only=>[:new,:create,:edit,:destroy,:update,:index]

  # GET /e_readings
  # GET /e_readings.xml
  def index
    @e_readings = EReading.all

    render :layout => "admin"
    #respond_to do |format|
    #  format.html # index.html.erb
    #  format.xml  { render :xml => @e_readings }
    #end
  end

  # GET /e_readings/1
  # GET /e_readings/1.xml
  def show
    @e_reading = EReading.find(params[:id])
    @best_sellers = BestSeller.get(7)
    @notices = Notice.get_notices(6)

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @e_reading }
    end
  end

  # GET /e_readings/new
  # GET /e_readings/new.xml
  def new
    @e_reading = EReading.new

    render :layout => "admin"
    #respond_to do |format|
    #  format.html # new.html.erb
    #  format.xml  { render :xml => @e_reading }
    #end
  end

  # GET /e_readings/1/edit
  def edit
    @e_reading = EReading.find(params[:id])
  end

  # POST /e_readings
  # POST /e_readings.xml
  def create
    @e_reading = EReading.new(params[:e_reading])

    respond_to do |format|
      if @e_reading.save
        format.html { redirect_to(@e_reading, :notice => 'E reading was successfully created.') }
        format.xml  { render :xml => @e_reading, :status => :created, :location => @e_reading }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @e_reading.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /e_readings/1
  # PUT /e_readings/1.xml
  def update
    @e_reading = EReading.find(params[:id])

    respond_to do |format|
      if @e_reading.update_attributes(params[:e_reading])
        format.html { redirect_to(@e_reading, :notice => 'E reading was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @e_reading.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /e_readings/1
  # DELETE /e_readings/1.xml
  def destroy
    @e_reading = EReading.find(params[:id])
    @e_reading.destroy

    respond_to do |format|
      format.html { redirect_to(e_readings_url) }
      format.xml  { head :ok }
    end
  end
end
