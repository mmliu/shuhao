class PageController < ApplicationController
  before_filter :authenticate,:only => [:admin]

  def home
    @classic_products = get_classic_books()

    news = News.get_news(1)
    @head_news = news[0]

    @news_list = Event.get_events(6)

    #@books_with_review = Product.with_review(4)
    @best_sellers = BestSeller.get(7)
    @book_flows = BookFlow.get(5)
    #@withReview = Product.pickOneWithReview

    @notices = Notice.get_notices(6)


    @latest_6_books = Product.get(6)

    #@brand_books = Brand.fetch
    @brands = Brand.all
  end

  def admin
    render :layout => "admin"
  end

  private
    def get_classic_books
      #TODO
      Product.all
    end

end
