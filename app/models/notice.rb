class Notice < ActiveRecord::Base
  attr_accessible :title, :content

  def self.get_notices(count)
    Notice.order("created_at DESC").all[0...count]
  end
end
