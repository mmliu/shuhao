#!/bin/env ruby
## encoding: utf-8
class Doc < ActiveRecord::Base
  attr_accessible :file, :name
  has_attached_file :file,
    :path => ":rails_root/public/system/:attachment/:id/:style/:filename",
    :url => "/system/:attachment/:id/:style/:filename"

  validates :name,:file, :presence => true
  #validates_attachment :file, :size => { :in => 0..800.kilobytes }
  validate :check_file_size

  private 
  def check_file_size
    logger.info self.file_file_size
    logger.info 1000.kilobytes
    if self.file_file_size > 1000.kilobytes
       errors.add("", '亲，文件别超过1M,不然羸弱的服务器君承受不了啊')
    end
  end
end

# == Schema Information
#
# Table name: docs
#
#  id                :integer         not null, primary key
#  file_file_name    :string(255)
#  file_content_type :string(255)
#  file_file_size    :integer
#  file_updated_at   :datetime
#  created_at        :datetime
#  updated_at        :datetime
#  name              :string(255)
#

