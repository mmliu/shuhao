class Publisher < ActiveRecord::Base
  has_many :products,:dependent => :destroy
end

# == Schema Information
#
# Table name: publishers
#
#  id         :integer         not null, primary key
#  name       :string(255)
#  created_at :datetime
#  updated_at :datetime
#

