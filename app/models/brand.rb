class Brand < ActiveRecord::Base
  has_many :products

  attr_accessible :image, :name, :description, :story
  has_attached_file :image, :styles => { :medium => "300x300>", :thumb => "100x100>" },
    :path => ":rails_root/public/system/:attachment/:id/:style/:filename",
    :url => "/system/:attachment/:id/:style/:filename"

  # pick one book from each brand
  def self.fetch
    Brand.all.each.map do |b|
      b.products.order('created_at DESC').first
    end
  end
end


# == Schema Information
#
# Table name: brands
#
#  id                 :integer         not null, primary key
#  name               :string(255)
#  created_at         :datetime
#  updated_at         :datetime
#  image_file_name    :string(255)
#  image_content_type :string(255)
#  image_file_size    :integer
#  image_updated_at   :datetime
#  description        :string(255)
#

