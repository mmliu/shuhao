class Event < ActiveRecord::Base
  attr_accessible :image, :title, :content
  has_attached_file :image, :styles => { :medium => "300x300>", :thumb => "100x100>" },
        :path => ":rails_root/public/system/:attachment/:id/:style/:filename",
        :url => "/system/:attachment/:id/:style/:filename"

  def self.get_events(count)
    Event.order("created_at DESC").all[0...count]    
  end
end

# == Schema Information
#
# Table name: events
#
#  id                 :integer         not null, primary key
#  title              :string(255)
#  content            :text
#  created_at         :datetime
#  updated_at         :datetime
#  image_file_size    :integer
#  image_file_name    :string(255)
#  image_content_type :string(255)
#  image_updated_at   :datetime
#

