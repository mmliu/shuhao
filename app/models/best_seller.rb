class BestSeller < ActiveRecord::Base
  belongs_to :product,:foreign_key=>"book_id"

  def self.get(count)
    BestSeller.order("created_at ASC")[0...count].map{|bs|
      book = bs.product
      if book!=nil then
        book
      end
    }
  end
end

# == Schema Information
#
# Table name: best_sellers
#
#  id         :integer         not null, primary key
#  book_id    :integer
#  created_at :datetime
#  updated_at :datetime
#

