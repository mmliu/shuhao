class Book < ActiveRecord::Base
end

# == Schema Information
#
# Table name: books
#
#  id         :integer         not null, primary key
#  title      :string(255)
#  intro      :text
#  price      :float
#  publisher  :string(255)
#  pubdate    :date
#  created_at :datetime
#  updated_at :datetime
#

