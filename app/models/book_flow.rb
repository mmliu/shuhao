#!/bin/env ruby
## encoding: utf-8
class BookFlow < ActiveRecord::Base
  belongs_to :product
  validate :check_book

  has_attached_file :image, :styles => { :medium => "300x300>", :thumb => "100x100>" },
    :path => ":rails_root/public/system/:attachment/:id/:style/:filename",
    :url => "/system/:attachment/:id/:style/:filename"


  def self.get(count)
    BookFlow.order("id DESC").where("image_file_name IS NOT NULL")[0...count]
  end

  private
  def check_book
    if !Product.exists?(self.product_id)
      errors.add(:product_id, "填写的图书编号没有对应的书目, :-(")
    end
  end
end


# == Schema Information
#
# Table name: book_flows
#
#  id                 :integer         not null, primary key
#  product_id         :integer
#  created_at         :datetime
#  updated_at         :datetime
#  image_file_name    :string(255)
#  image_content_type :string(255)
#  image_file_size    :integer
#  image_updated_at   :datetime
#

