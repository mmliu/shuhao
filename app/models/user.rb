class User < ActiveRecord::Base
  before_save :encrypt_password

  attr_accessor :password
  attr_accessible :name,:password,:password_confirmation

  validates_confirmation_of :password

  validates_presence_of :password
  validates_length_of :password,:within=>6..40

  def has_password?(pwd_submitted)
    encrypted_password == encrypt(pwd_submitted)
  end

  def self.authenticate(name,pwd_submitted)
    user =  User.find_by_name(name)
    return nil if user.nil?
    return user if user.has_password?(pwd_submitted)
  end

  def self.authenticate_with_salt(id,salt)
    user = find_by_id(id)
    (user && user.salt == salt)? user:nil
  end

  private
    
    def encrypt_password
      self.salt = make_salt
      self.encrypted_password = encrypt password
    end

    def encrypt(string)
      secure_hash "#{salt}#{string}" #TODO
    end

    def make_salt
      secure_hash "#{Time.now.utc}#{password}"
    end

    def secure_hash(string)
      Digest::SHA2::hexdigest(string)
    end
end


# == Schema Information
#
# Table name: users
#
#  id                 :integer         not null, primary key
#  name               :string(255)
#  created_at         :datetime
#  updated_at         :datetime
#  encrypted_password :string(255)
#  salt               :string(255)
#

