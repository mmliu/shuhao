class Category < ActiveRecord::Base
  attr_accessible :image, :name, :story
  has_attached_file :image, :styles => { :medium => "300x300>", :thumb => "100x100>" },
    :path => ":rails_root/public/system/:attachment/:id/:style/:filename",
    :url => "/system/:attachment/:id/:style/:filename"

  has_many :products,:dependent => :destroy

  def get_products_for_nsbox(num=9)
    result = Product.where("category_id = ?",self.id)[0...num]
    #if result is shorter than required,fill up with nil
    if result.length < num
      result+[nil]*(num - result.length)
    else
      result
    end
  end
end

# == Schema Information
#
# Table name: categories
#
#  id         :integer         not null, primary key
#  name       :string(255)
#  created_at :datetime
#  updated_at :datetime
#

# == Schema Information
#
# Table name: categories
#
#  id         :integer         not null, primary key
#  name       :string(255)
#  created_at :datetime
#  updated_at :datetime
#

