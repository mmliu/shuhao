class EReading < ActiveRecord::Base
  validates :name, :url, :avatar, :presence => true
  has_attached_file :avatar, :styles => { :medium => "300x300>", :thumb => "100x100>" },
      :path => ":rails_root/public/system/:attachment/:id/:style/:filename",
      :url => "/system/:attachment/:id/:style/:filename"

  def self.top(num)
    EReading.order("updated_at DESC").limit(num)
  end
end


# == Schema Information
#
# Table name: e_readings
#
#  id                  :integer         not null, primary key
#  url                 :string(255)
#  name                :string(255)
#  created_at          :datetime
#  updated_at          :datetime
#  avatar_file_size    :integer
#  avatar_file_name    :string(255)
#  avatar_content_type :string(255)
#  avatar_updated_at   :datetime
#

