class Author < ActiveRecord::Base
  has_many :products,:dependent => :destroy


  def self.for_author_flow(num)
    Author.where("avatar_url IS NOT null")[0...num]
  end
end



# == Schema Information
#
# Table name: authors
#
#  id         :integer         not null, primary key
#  name       :string(255)
#  created_at :datetime
#  updated_at :datetime
#  avatar_url :string(255)
#

