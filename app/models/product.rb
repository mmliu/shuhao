class Product < ActiveRecord::Base
  belongs_to :author
  belongs_to :publisher
  belongs_to :category
  belongs_to :brand

  validates :price,:numericality=>{:greater_than_or_equal_to => 0.1}

  has_attached_file :image, :styles => { :medium => "300x300>", :thumb => "100x100>" },
         :path => ":rails_root/public/system/:attachment/:id/:style/:filename",
         :url => "/system/:attachment/:id/:style/:filename"

  def self.with_review(count)
    Product.order("created_at ASC").where("review IS NOT NULL")[0...count]
  end

  def self.pickOneWithReview
    #TODO TMP IMPL,add random
    with_review(1)[0]
  end

  def self.get(count)
    Product.order("created_at ASC")[0...count]
  end
end













# == Schema Information
#
# Table name: products
#
#  id                 :integer         not null, primary key
#  title              :string(255)
#  description        :string(255)
#  price              :decimal(, )
#  created_at         :datetime
#  updated_at         :datetime
#  promote_img_name   :string(255)
#  is_for_youth       :boolean         default(FALSE)
#  category_id        :integer
#  author_id          :integer
#  publisher_id       :integer
#  review             :text
#  dangdang_link      :string(255)
#  format             :string(255)
#  pubdate            :string(255)
#  page               :integer
#  brand_id           :integer
#  image_file_name    :string(255)
#  image_content_type :string(255)
#  image_file_size    :integer
#  image_updated_at   :datetime
#

