$(function() {
  setTimeout(updateAuthors, 10000);
});

function updateAuthors () {
  var author_id = $("#product_author_id option:last-child").val()
  $.getScript("/authors.js?last_id=" + author_id)
  setTimeout(updateAuthors, 10000);
}
