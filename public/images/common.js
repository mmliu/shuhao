	///
	// Title: forbidRightClick <p>
	// Description: This function is used to forbid mouseright click.<p>
	// Copyright:  
	// Company: <p>
	// @author: sunsnow
	// @version 1.0
	// @date    2004/09/09
	// <param name=""></param>
	// <return value="true">allow</return>
	// <return value="false">forbid</return>
	///
	function forbidRightClick(){
		return false;
	}

  function confirmDel(){						
		if (confirm("确认要删除？")){
			return true;
		}else{
			return false;
		}
	}
	function trim(str){
		str = str.toString()
		var index = str.indexOf(" ")
		if(index == -1 || str.length == 0)
			return str
		
		if(index == 0){
			while(index == 0){
				str = str.replace(" ","")
				index = str.indexOf(" ")
			}
		}
		index = str.indexOf(" ",str.length-1)
		
		if(index == str.length -1 && str.length != 0){
			while(index == str.length-1){
				str = str.substring(0,index)
				index = str.indexOf(" ",str.length-1)
			}
		}
		return str
	}
    function Trim(str)
    {
        return str.replace(/(^s*)|(s*$)/g,'');
    }
    function LTrim(str)
    {
        return str.replace(/(^s*)/g,'');
    }
    function RTrim(str)
    {
        return str.replace(/(s*$)/g,'');
    }

function display_info(url,name){
	var newwin=window.open(url,name,"toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=640,height=480");
	newwin.focus();
	return true;

}
function openScript(url, width, height){
	var Win = window.open(url,"openScript",'width=' + width + ',height=' + height + ',resizable=yes,scrollbars=yes,menubar=no,status=yes' );
	Win.focus();
}

function openWin(url){
	var Win = window.open(url,"WinOpen",'' );
	Win.focus();
}
function showDialog_Detail(url){
	window.showModalDialog(url,"Dailog",'dialogWidth:800px;dialogHeight:600px;help:no;scroll:yes;status:no')	
}
function showDialog_Detail(url, width, height){
    //window.showModalDialog(url,"Dailog",'dialogWidth:' + width + ';dialogHeight:' + height + ';help:no;scroll:yes;status:no')	
    window.showModalDialog(url,"Dailog",'dialogWidth=' + width + 'px;dialogHeight=' + height + 'px;center=yes;status=no;scroll=yes;')	
}
/*
使用方法如下：
   vReturnValue = window.showModalDialog(sURL [, vArguments] [, sFeatures])
    vReturnValue = window.showModelessDialog(sURL [, vArguments] [, sFeatures])

参数说明：
    sURL
   必选参数，类型：字符串。用来指定对话框要显示的文档的URL。
    vArguments
   可选参数，类型：变体。用来向对话框传递参数。传递的参数类型不限，包括数组等。对话框通过window.dialogArguments来取得传递进来的参数。
    sFeatures
   可选参数，类型：字符串。用来描述对话框的外观等信息，可以使用以下的一个或几个，用分号“;”隔开。
    dialogHeight 对话框高度，不小于１００px，ＩＥ４中dialogHeight 和 dialogWidth 默认的单位是em，而ＩＥ５中是px，为方便其见，在定义modal方式的对话框时，用px做单位。
 　 dialogWidth: 对话框宽度。
 　 dialogLeft: 距离桌面左的距离。
 　 dialogTop: 离桌面上的距离。
 　 center: {yes | no | 1 | 0 }：窗口是否居中，默认yes，但仍可以指定高度和宽度。
 　 help: {yes | no | 1 | 0 }：是否显示帮助按钮，默认yes。
 　 resizable: {yes | no | 1 | 0 } ［ＩＥ５＋］：是否可被改变大小。默认no。
 　 status: {yes | no | 1 | 0 } ［IE5+］：是否显示状态栏。默认为yes[ Modeless]或no[Modal]。
    scroll:{ yes | no | 1 | 0 | on | off }：指明对话框是否显示滚动条。默认为yes。

还有几个属性是用在HTA中的，在一般的网页中一般不使用。
    dialogHide:{ yes | no | 1 | 0 | on | off }：在打印或者打印预览时对话框是否隐藏。默认为no。
    edge:{ sunken | raised }：指明对话框的边框样式。默认为raised。
    unadorned:{ yes | no | 1 | 0 | on | off }：默认为no。
*/

var isNN = (navigator.appName.indexOf("Netscape")!=-1);
function containsElement(arr, ele) { 
  var found = false, index = 0; 
  while(!found && index < arr.length) 
    if(arr[index] == ele) 
      found = true; 
    else 
      index++; 
  return found; 
} 
function getIndex(input) { 
  var index = -1, i = 0, found = false; 
  while (i < input.form.length && index == -1) 
    if (input.form[i] == input) 
      index = i; 
    else 
      i++; 
  return index; 
} 

function autoTab(input,len, a) { 
  var e = a ? a:window.event;
  var keyCode = e.keyCode ? e.keyCode : e.which ? e.which : e.charCode; 
  var filter = (isNN) ? [0,8,9] : [0,8,9,16,17,18,37,38,39,40,46]; 
  if ((input.value.length >= len && !containsElement(filter,keyCode)) || keyCode == 13) { 
    input.value = input.value.slice(0, len); 
    input.form[(getIndex(input)+1) % input.form.length].focus(); 
  } 
  //return true; 
}
function enterTab(input, msg, e) { 
  if (e == null){
    e = window.event;
  }
  var keyCode = e.keyCode ? e.keyCode : e.which ? e.which : e.charCode; 
  if (keyCode == 13){
    if (msg == ""){
      if (e.keyCode){
        e.keyCode = 9;
      }else if (e.which){
        e.which = 9;
      }else{
        e.charCode = 9;
      }
    }else if (trim(input.value) == ""){
      alert(msg);
      return false;
    }else{
      if (e.keyCode){
        e.keyCode = 9;
      }else if (e.which){
        e.which = 9;
      }else{
        e.charCode = 9;
      }
    }
  }
  alert(0);
} 
function enterTab(input, msg) { 
  var e = event;
  if (e == null){
    e = window.event;
  }
  var keyCode = e.keyCode ? e.keyCode : e.which ? e.which : e.charCode; 
  if (keyCode == 13){
    if (msg == ""){
      if (e.keyCode){
        e.keyCode = 9;
      }else if (e.which){
        e.which = 9;
      }else{
        e.charCode = 9;
      }
    }else if (trim(input.value) == ""){
      alert(msg);
      return false;
    }else{
      if (e.keyCode){
        e.keyCode = 9;
      }else if (e.which){
        e.which = 9;
      }else{
        e.charCode = 9;
      }
    }
  }
} 
function enterTabList() { 
  var e = event;
  if (e == null){
    e = window.event;
  }
  var keyCode = e.keyCode ? e.keyCode : e.which ? e.which : e.charCode; 
  if (keyCode == 13){
    e.keyCode = 9; 
  }
}

function Show_Hide(_this,_divInfo,_title){
  var divgoodsinfo = document.getElementById(_divInfo);
  if (divgoodsinfo.style.display == "none"){
    divgoodsinfo.style.display = "block";
    _this.value = "隐含" + _title;
  }else{
    divgoodsinfo.style.display = "none";
    _this.value = "显示" + _title;
  }
}

function Show_Hide_DivS(_this,_divInfo,_title){
  var divgoodsinfo = document.getElementById(_divInfo);
  if (divgoodsinfo.style.display == "none"){
    divgoodsinfo.style.display = "";
    _this.innerHTML = "隐含" + _title;
  }else{
    divgoodsinfo.style.display = "none";
    _this.innerHTML = "显示" + _title;
  }
}


/*  
*    ForDight(Dight,How):数值格式化函数，Dight要  
*    格式化的  数字，How要保留的小数位数。  
*/  
function  ForDight(Dight,How){  
    Dight = Math.round(Dight * Math.pow(10,How))/Math.pow(10,How);  
    return Dight;  
}  
function keyPressed(TB){ 
    keyPressed2(TB, 0, event);
}

function keyPressed1(TB, e){ 
    keyPressed2(TB, LockCols, e);
}
function keyPressed2(TB, LockCols, e){ 
    var TBID = document.getElementById(TB);
    var CurCellID = TBID.parentNode;
    var tblGrid = CurCellID.parentNode.parentNode;
    var CurRowIndex = CurCellID.parentNode.rowIndex;
    var CurCellIndex = CurCellID.cellIndex;
    var rowcount = tblGrid.rows.length; 
    var CellCount = tblGrid.rows[CurRowIndex].cells.length;
    var TargateRow=-1 ;
    var TargateCell=-1 ;
    var R,C; 
    if (e == null){
        e = window.event;
    }
    var keyCode = e.keyCode ? e.keyCode : e.which ? e.which : e.charCode;     
    if (keyCode == 38 || keyCode == 40 ) { 
        if (tblGrid.rows[CurRowIndex].cells[CurCellIndex + LockCols].childNodes[0] != null){ 
            if (tblGrid.rows[CurRowIndex].cells[CurCellIndex + LockCols].childNodes[0].id == TBID.id) {
                if (keyCode == 40 )//Buttom 
                {     
                    if (CurRowIndex < rowcount-1) 
                    { 
                        if (tblGrid.rows[CurRowIndex+1].cells[CurCellIndex + LockCols].childNodes[0] != null) 
                        {                    
                            if (tblGrid.rows[CurRowIndex+1].cells[CurCellIndex + LockCols].childNodes[0].type == 'text') 
                            { 
                                //downvalue 
                                tblGrid.rows[CurRowIndex+1].cells[CurCellIndex + LockCols].childNodes[0].focus();	
                                return false;
                            } 
                        } 
                    }
                } 
                if (keyCode == 38 && CurRowIndex > 0 )//up
                { 
                    if (tblGrid.rows[CurRowIndex - 1].cells[CurCellIndex + LockCols].childNodes[0] != null)
                    { 
                        if (tblGrid.rows[CurRowIndex - 1].cells[CurCellIndex + LockCols].childNodes[0].type == 'text')
                        {
                            //upvalue 
                            tblGrid.rows[CurRowIndex - 1].cells[CurCellIndex + LockCols].childNodes[0].focus(); 
                            return false; 
                        } 
                    } 
                } 
            }
        }
    }
}

    /*比较日期*/
    function  DateDiff(sDate1,sDate2) {    
        var aDate,oDate1,oDate2,iDays     
        aDate=sDate1.split("-")     
        oDate1=new   Date(aDate[1]+ '-'+aDate[2]+ '-'+aDate[0])         //转换为12-18-2002格式     
        aDate=sDate2.split("-")     
        oDate2=new  Date(aDate[1]+ '-'+aDate[2]+ '-'+aDate[0])     
        iDays  = parseInt(Math.abs(oDate1-oDate2)/1000/60/60/24)         //把相差的毫秒数转换为天数     
        if(oDate1 <oDate2) { 
            return  iDays; 
        } else { 
            return  -iDays ; 
         } 
    } 

//这个可以验证15位和18位的身份证，并且包含生日和校验位的验证。   
//如果有兴趣，还可以加上身份证所在地的验证，就是前6位有些数字合法有些数字不合法。  
function isIdCardNo(num) {             
    num = num.toUpperCase();           
    //身份证号码为15位或者18位，15位时全为数字，18位前17位为数字，最后一位是校验位，可能为数字或字符X。             
    if (!(/(^\d{15}$)|(^\d{17}([0-9]|X)$)/.test(num))){                
        //alert('输入的身份证号长度不对，或者号码不符合规定！\n15位号码应全为数字，18位号码末位可以为数字或X。');               
        return false;          
    } 
    //校验位按照ISO 7064:1983.MOD 11-2的规定生成，X可以认为是数字10。 
    //下面分别分析出生日期和校验位 
    var len, re; 
    len = num.length; 
    if (len == 15) { 
        re = new RegExp(/^(\d{6})(\d{2})(\d{2})(\d{2})(\d{3})$/); 
        var arrSplit = num.match(re);  
        //检查生日日期是否正确 
        var dtmBirth = new Date('19' + arrSplit[2] + '/' + arrSplit[3] + '/' + arrSplit[4]); 
        var bGoodDay; 
        bGoodDay = (dtmBirth.getYear() == Number(arrSplit[2])) && ((dtmBirth.getMonth() + 1) == Number(arrSplit[3])) && (dtmBirth.getDate() == Number(arrSplit[4])); 
        if (!bGoodDay) {           
            //alert('输入的身份证号里出生日期不对！');              
            return false; 
        } else { 
            //将15位身份证转成18位 //校验位按照ISO 7064:1983.MOD 11-2的规定生成，X可以认为是数字10。         
            var arrInt = new Array(7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2);            
            var arrCh = new Array('1', '0', 'X', '9', '8', '7', '6', '5', '4', '3', '2');            
            var nTemp = 0, i;               
            num = num.substr(0, 6) + '19' + num.substr(6, num.length - 6);            
            for(i = 0; i < 17; i ++)  {                 
                nTemp += num.substr(i, 1) * arrInt[i];            
            }           
            num += arrCh[nTemp % 11];               
            return num;   
        }   
    } 
    if (len == 18) {
        re = new RegExp(/^(\d{6})(\d{4})(\d{2})(\d{2})(\d{3})([0-9]|X)$/); 
        var arrSplit = num.match(re);  
        //检查生日日期是否正确
        var dtmBirth = new Date(arrSplit[2] + "/" + arrSplit[3] + "/" + arrSplit[4]); 
        var bGoodDay; bGoodDay = (dtmBirth.getFullYear() == Number(arrSplit[2])) && ((dtmBirth.getMonth() + 1) == Number(arrSplit[3])) && (dtmBirth.getDate() == Number(arrSplit[4])); 
        if (!bGoodDay) { 
            //alert(dtmBirth.getYear()); 
            //alert(arrSplit[2]); 
            //alert('输入的身份证号里出生日期不对！'); 
            return false; 
        } else { 
            //检验18位身份证的校验码是否正确。 //校验位按照ISO 7064:1983.MOD 11-2的规定生成，X可以认为是数字10。 
            var valnum; 
            var arrInt = new Array(7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2); 
            var arrCh = new Array('1', '0', 'X', '9', '8', '7', '6', '5', '4', '3', '2'); 
            var nTemp = 0, i; 
            for(i = 0; i < 17; i ++) {
                nTemp += num.substr(i, 1) * arrInt[i]; 
            } 
            valnum = arrCh[nTemp % 11]; 
            if (valnum != num.substr(17, 1)) { 
                //alert('18位身份证的校验码不正确！应该为：' + valnum); 
                return false; 
            }
            return num; 
        } 
    } 
    return false;
}  