﻿//
function FocusA6() {
}
//flash的路径
FocusA6.prototype.Path="/dangdang/focus_A6.swf";
//flash中图片的宽高
FocusA6.prototype.ImgWidth=200;
FocusA6.prototype.ImgHeight=300;
//导航条位置,仅限bottom与top两个值,默认为bottom。
FocusA6.prototype.NavPlace="bottom";
//文字区域的宽高
FocusA6.prototype.TxtHeight=100;
//字体的背景色
FocusA6.prototype.TxtBackColor=0xffffff;
//字体背景透明度
FocusA6.prototype.TxtBackAlpha=60;
//标题文字颜色
FocusA6.prototype.TitleColor=0x00ff00;
//简介文字的大小
FocusA6.prototype.TxtFontSize=14;
//简介文字字体颜色
FocusA6.prototype.TxtFontColor=0x999999;

//图片切换间隔帧数,单位为秒
FocusA6.prototype.ReturnNum = 5;
//图片地址
FocusA6.prototype.Pics="";
//新闻链接地址
FocusA6.prototype.Urls="";
//标题
FocusA6.prototype.Titles="";
//内容简介
FocusA6.prototype.Intros="";
//
//输出flash
FocusA6.prototype.write=function() {
    var str="<embed flashvars=\"imgWidth="+this.ImgWidth+"&imgHeight="+this.ImgHeight+
    "&txtBackColor="+this.TxtBackColor+"&txtBackAlpha="+this.TxtBackAlpha+
    "&titleColor="+this.TitleColor+"&navPlace="+this.NavPlace+
    "&txtHeight="+this.TxtHeight+"&txtFontSize="+this.TxtFontSize+"&txtFontColor="+this.TxtFontColor+
    "&pics="+this.Pics+"&titles="+this.Titles+"&intros="+this.Intros+
    "&urls="+this.Urls+"&returnNum="+this.ReturnNum+"\" "+ 
    "wmode=\"transparent\" "+
    "src=\""+this.Path+"\" quality=\"high\" width=\""+this.ImgWidth+"\" height=\""+(this.ImgHeight+this.TxtHeight+25)+"\" name=\"FocusA6\"  id=\"FocusA6\" align=\"middle\" allowScriptAccess=\"sameDomain\" type=\"application/x-shockwave-flash\" pluginspage=\"http://www.macromedia.com/go/getflashplayer\" />";
    //alert(str)
	document.write(str);
}