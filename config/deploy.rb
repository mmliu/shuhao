set :application, "shuhao"
set :repository,  "git@bitbucket.org:mmliu/shuhao.git"

set :scm, :git # You can set :scm explicitly or Capistrano will make an intelligent guess based on known version control directory names
# Or: `accurev`, `bzr`, `cvs`, `darcs`, `git`, `mercurial`, `perforce`, `subversion` or `none`
set :branch, "master"
set :deploy_via, :remote_cache

role :web, "ec2"                          # Your HTTP server, Apache/etc
role :app, "ec2"                          # This may be the same as your `Web` server
role :db,  "ec2", :primary => true # This is where Rails migrations will run
# role :db,  "your slave db-server here"

set(:deploy_to) { "/home/ec2-user/apps/shuhao" }

# if you want to clean up old releases on each deploy uncomment this:
# after "deploy:restart", "deploy:cleanup"

# if you're still using the script/reaper helper you will need
# these http://github.com/rails/irs_process_scripts

# If you are using Passenger mod_rails uncomment this:
# namespace :deploy do
#   task :start do ; end
#   task :stop do ; end
#   task :restart, :roles => :app, :except => { :no_release => true } do
#     run "#{try_sudo} touch #{File.join(current_path,'tmp','restart.txt')}"
#   end
# end

default_run_options[:pty] = true  # Must be set for the password prompt
                                  # from git to work

set :user, "ec2-user"  # The server's user for deploys
set :admin_runner, 'root'
#set :user, "feihong"  # The server's user for deploys

# If you're using your own private keys for git, you want to tell Capistrano to use agent forwarding with this command.
# Agent forwarding can make key management much simpler as it uses your local keys instead of keys installed on the server.
ssh_options[:forward_agent] = true

set :rails_env, :production
set :rake_bin, "/usr/local/bin/rake"
set(:unicorn_binary) { "unicorn" }#"/usr/local/bin/unicorn"
set(:unicorn_pid) { "#{current_path}/tmp/pids/unicorn.pid" }
set(:unicorn_config) { "#{current_path}/config/unicorn_production.rb" }

# If you are using Passenger mod_rails uncomment this:
# namespace :deploy do #   task :start do ; end
#   task :stop do ; end
#   task :restart, :roles => :app, :except => { :no_release => true } do
#     run "#{try_sudo} touch #{File.join(current_path,'tmp','restart.txt')}"
#   end
# end
namespace :deploy do
  desc "start shuhao service"
  task :start do
    unicorn.start
  end

  desc "stop shuhao service"
  task :stop do
    unicorn.stop
  end

  desc "install bundled gems"
  task :do_tasks_after_update do
    bundle_install
    make_symlink_files
    # precompile_assets
  end

  desc "install bundled gems"
  task :bundle_install do
    begin
      run "cd #{current_path} && bundle check"
    rescue => e
      #run "cd #{current_path} && bundle install --deployment"
      #ignore rubygems.org, fuck gfw
      run "cd #{current_path} && bundle install --deployment"
    end
  end

  desc "Link shared config files"
  task :make_symlink_files do
    run "ln -s #{shared_path}/config/unicorn_production.rb #{release_path}/config/unicorn_production.rb"
    run "ln -s #{shared_path}/config/database.yml #{release_path}/config/database.yml"
  end

  desc "Precompile assets"
  task :precompile_assets do
    run "cd #{current_path} && bundle exec rake assets:precompile"
  end

  desc "restart shuhao service"
  task :restart, :roles => [:app, :worker], :except => { :no_release => true } do
    unicorn.restart
  end
end

after "deploy:update", "deploy:do_tasks_after_update"
after "deploy", "deploy:migrate"

namespace :unicorn do
  desc "start unicorn"
  task :start, :roles => :app, :except => { :no_release => true } do
    run "cd #{current_path} && bundle exec #{unicorn_binary} -c #{unicorn_config} -E #{rails_env} -D"
  end

  desc "stop unicorn"
  task :stop, :roles => :app, :except => { :no_release => true } do
    begin
      run "cd #{current_path} && [ -f #{unicorn_pid} ] && kill `cat #{unicorn_pid}`"
    rescue => e
      puts "==> Can't stop unicorn. Maybe there's no such file #{unicorn_pid} exists."
    end
  end

  desc "graceful stop unicorn"
  task :graceful_stop, :except => { :no_release => true } do
    run "kill -s QUIT `cat #{unicorn_pid}`"
  end

  desc "reload unicorn"
  task :reload, :except => { :no_release => true } do
    begin
      run "kill -s USR2 `cat #{unicorn_pid}`"
    rescue => e
      puts "==> It seems that unicorn is not started yet. Trying cold start..."
      start
    end
  end

  task :restart, :roles => :app, :except => { :no_release => true } do
    stop
    start
  end
end
