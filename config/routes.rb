BookStore::Application.routes.draw do

  get "notices/admin"
  resources :notices

  get "docs/admin"

  resources :docs

  get "events/admin"
  resources :events

  resources :brands

  resources :e_readings

  get "book_flow/new"

  get "book_flow/create"

  get "book_flow/edit"

  get "book_flow/update"

  get "book_flow/destroy"

  get "book_flow/show"

  get "book_flow/index"

  get "sessions/new"


  resources :sessions,:only=>[:new,:create,:destroy]

  match '/signup',:to=>'users#new'
  match '/signin',:to => 'sessions#new'
  match '/signout',:to=> 'sessions#destroy'
  match '/search',:to=>'products#search'
  match '/admin', :to => 'page#admin'

  resources :book_flows do

  end

  resources :users

  resources :best_sellers

  get "news/admin"
  resources :news

  resources :publishers

  resources :authors

  resources :categories

  get "products/book_list"

  resources :products


  get "books/index"

  get "books/show"

  get "books/destroy"

  get "books/create"

  get "books/new"

  get "book/index"

  get "book/new"

  get "book/destroy"

  root :to=> "page#home"
# root :to => "welcome#index"
  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  # root :to => "welcome#index"

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id(.:format)))'
end
